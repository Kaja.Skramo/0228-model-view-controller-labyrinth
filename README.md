# Addendum

På slutten av forelesning fikk vi akkurat ikke tid til å trykke på pil ned for å flytte spilleren. Det jeg hadde glemt var en "magisk" linje i konstruktøren til `View` som gjør den i stand til å motta tastetrykk fra operativsystemet:
```java
this.setFocusable(true)
```

Metoden setFocusable er arvet fra JComponent, og gjør at vinduet kan få "fokus," og dermed også motta tastetrykk og musebevegelser fra operativsystemet.


Legg også merke til at vi oppretter kontrolleren i `Main`.